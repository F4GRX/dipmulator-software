#include <Wire.h>

// faster i2c:
//  To get that, you'll have to set the prescaler bits TWSP0 and TWSP1 to 0 (1:1 prescale ratio), and the TWBR register to 1 (1:1 ratio of SCL to the prescaler's output).
// cbi(TWSR, TWPS0);
// cbi(TWSR, TWPS1);
// TWBR = ((F_CPU / TWI_FREQ) - 16) / 2;
  /* twi bit rate formula from atmega128 manual pg 204
  SCL Frequency = CPU Clock Frequency / (16 + (2 * TWBR))
  note: TWBR should be 10 or higher for master mode
  It is 72 for a 16mhz Wiring board with 100kHz TWI */

char inbuf[64];
uint8_t inbuflen;
  
uint8_t data_buf[16]; //at most 16 bytes per s-record/ihex record
uint8_t data_len;     //number of bytes in this buffer
uint16_t data_addr;   //where to load these bytes in the EPROM

uint16_t destaddr;    //Address where to write in EPROM
uint8_t destdata;     //Data to read/write in EPROM

/* GPIO mapping 
 * IO Expander 1 address 0x36 (0011 0110) -> 0x1B (001 1011)
 * GPA0  Spare
 * GPA1  Spare
 * GPA2  Spare
 * GPA3  Spare
 * GPA4  Spare
 * GPA5  MEM_RD
 * GPA6  MEM_WR
 * GPA7  MEM_CE
 * GPB0  A8
 * GPB1  A9
 * GPB2  A10
 * GPB3  A11
 * GPB4  A13
 * GPB5  A14
 * GPB6  A12
 * GPB7  BUSEN
 *
 * IO Expander 2 0x38 (0011 1000) -> 0x1C (001 1100)
 * GPA0  D3
 * GPA1  D4
 * GPA2  D5
 * GPA3  D6
 * GPA4  D7
 * GPA5  D0
 * GPA6  D1
 * GPA7  D2
 * GPB0  A7
 * GPB1  A4
 * GPB2  A6
 * GPB3  A5
 * GPB4  A3
 * GPB5  A1
 * GPB6  A2
 * GPB7  A0
 *
 * Mapping of EPROM bus bits to GPIO ports
 * EPROM    Device    Pin    SRAM pin
 * A0       2         B7
 * A1       2         B5
 * A2       2         B6
 * A3       2         B4
 * A4       2         B1
 * A5       2         B3
 * A6       2         B2
 * A7       2         B0
 * A8       1         B0
 * A9       1         B1
 * A10      1         B2
 * A11      1         B3
 * A12      1         B6
 * A13      1         B4
 * A14      1         B5
 * D0       2         A5
 * D1       2         A6
 * D2       2         A7
 * D3       2         A0
 * D4       2         A1
 * D5       2         A2
 * D6       2         A3
 * D7       2         A4
 * MEM_RD   1         A5
 * MEM_WR   1         A6
 * MEM_CE   1         A7
 * BUSEN    1         B7
 */

uint8_t gpio1a;       //Port A bits in MCP device 0x36
uint8_t gpio1b;       //Port B bits in MCP device 0x36

uint8_t gpio2a;       //Port A bits in MCP device 0x38 (address bits)
uint8_t gpio2b;       //Port B bits in MCP device 0x38 (data bits)

uint8_t state;
#define STATE_WAIT  0 //waiting for S or :
#define STATE_SREC  1 //got a S, will parse s-record
#define STATE_IHEX  2 //got a :, will parse intel-hex

void setup() {
  Serial.begin (115200);

  // Leonardo: wait for serial port to connect
  while (!Serial) 
    {
    }

  state = STATE_WAIT;
  
  Serial.println("dipmulator 1.0 ready. Accepting s-record and intelhex");

}  // end of setup

/*S-record structure:
 * STNNAAAADDSS
 * T type 1 (data, 16-bit addr) 2 (data, 24-bit addr) 3 (data, 32-bit addr) other discarded
 * NN byte count
 * AAAA address
 * DD data
 * SS checksum
 */
void do_srec(void) {
  Serial.print("parse srec len=");
  Serial.println(inbuflen, DEC);
  Serial.println(inbuf);
}

/*intelhex record structure:
 * :NNAAAATTDDSS
 * NN byte count
 * AAAA address
 * TT type (00 data, 04 address MSW, others discarded)
 * DD data (2 bytes of address MSW big-endian for type 4)
 * SS checksum (all decoded bytes sums to zero)
 */
void do_ihex(void) {
  Serial.print("parse ihex len=");
  Serial.println(inbuflen, DEC);
  Serial.println(inbuf);
}

void loop() {
  if(Serial.available()<1) {
    return;
  }
  char ch = Serial.read();
  
  if(state == STATE_WAIT) {
    if(ch == ':') {
      state = STATE_IHEX;
      inbuflen = 0;
    } else if(ch == 'S') {
      state = STATE_SREC;
      inbuflen = 0;
    }
  } else if(state == STATE_SREC || state == STATE_IHEX) {    
    if(ch == 0x0D || ch == 0x0A) {
      inbuf[inbuflen++] = 0;
      //done, parse
      if(state == STATE_SREC) do_srec();
      else                    do_ihex();
      state = STATE_WAIT;
    } else {
      if(inbuflen < sizeof(inbuf)) {
        inbuf[inbuflen++] = ch;
      }    
    }
  } else {
    //unknown state!
  }
  
  // Parse it

}
